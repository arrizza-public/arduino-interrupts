import serial

from tools.xplat_utils.os_specific import OsSpecific

# --------------------
## run automated test
class App:
    # --------------------
    ## constructor
    def __init__(self):
        ## hold serial object
        self._ser = None

    # --------------------
    ## initialize
    #
    # @return None
    def init(self):
        OsSpecific.init()
        if OsSpecific.os_name == 'macos':
            port = '/dev/tty.usbserial-113240'
        else:
            port = '/dev/ttyUSB0'
        self._ser = serial.Serial(port,
                                  baudrate=115200,
                                  bytesize=serial.EIGHTBITS,
                                  stopbits=serial.STOPBITS_ONE,
                                  parity=serial.PARITY_NONE,
                                  timeout=None)
        self._ser.flush()

    # --------------------
    ## terminate
    #
    # @return None
    def term(self):
        print()

        # close it
        self._ser.close()

    # --------------------
    ## run automated test
    #
    # @return None
    def run(self):
        # reset the read_time to 1s
        self._ser.timeout = 1.0

        # read a couple of times to get rid of garbage
        self._ser.write(0x0D)
        while True:
            ch = self._ser.read(1)
            if not ch or ch == 0x0A:
                break

        # must match the order of the values in the prescaler array
        prescaler_codes = [0b111, 0b110, 0b101, 0b100, 0b011, 0b010, 0b001]
        prescaler_multi = [1024, 256, 128, 64, 32, 8, 1]
        # setup the various prescalers
        for i in [0, 1, 2, 3, 4, 5, 6]:
            prescaler = prescaler_codes[i]
            # set some OCR2A values for each prescaler
            for val in [1, 32, 63, 95, 127, 255]:
                # note: some combinations of prescalar and
                #       OCR2A values cause the communication to hang
                #       i.e. the ISR is taking too much of the MCU
                #       processing time to be able to process
                #       incoming/outgoing characters on the Serial Port

                # To fix:
                #   - change the ISR to be much more efficient
                #
                # To detect:
                #   - you'll see garbled input on your PC
                #   - usually occurs when frequency is ~36Khz or higher
                #
                # To recover, try this:
                #   - press reset button on Arduino
                #   - then press enter on your PC

                self._ser.flush()
                pulse_width = (val + 1) * ((1.0 / 16_000_000) * prescaler_multi[i])
                exp_freq = (1.0 / pulse_width) / 2.0
                msg = f'{prescaler:<03b} {val}'
                print(f'tx: SND {msg} pulse width:{pulse_width * 1_000:<0.3f}mS freq:{exp_freq: <0.3f}Hz')
                self._ser.write(msg.encode())
                self._ser.write(b'\x0D')
                self._read_response()

                ok = input('Press q or x to quit; enter when you\'re ready to continue! > ')
                if ok in ['q', 'x']:
                    import sys
                    sys.exit(0)


    # --------------------
    ## reads response from arduino
    #
    # @return None
    def _read_response(self):
        print('rx: ', end='')
        while True:
            # get the next byte
            ch = self._ser.read(size=1)

            # uncomment to debug
            # print(f'DBG ch:{ch} {ord(ch):02X}: ')

            # if we got nothing from the Arduino,
            # it may have timed out, try again
            if not ch:
                continue

            # the response ends on a linefeed from the Arduino
            if ord(ch) == 0x0A:
                break

            # print the character as a character (not a byte) on your PC
            if ord(ch) == 0x0D:
                print(' ', end='')
            else:
                print(f'{chr(ord(ch))}', end='')

        print('')


# --------------------
### MAIN
def main():
    app = App()
    app.init()
    app.run()
    app.term()


main()
