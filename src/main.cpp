// --------------------
//! @file
//! Copyright(c) All Rights Reserved
//!
//! Summary: Toggle a pin on a timer interrupt
// Use an oscilloscope to see the pin output and
// measure the frequency.

// cppcheck-suppress missingInclude
#include <avr/interrupt.h>
#include <wiring_private.h>

//! toggle this pin during the interrupt so it can be read with an oscope
static int pin_num = 10;
//! the current state of the pin
static int pin_state = 0;

// --------------------
//! interrupt service routine (ISR) is called when the timer interrupt
ISR(TIMER2_COMPA_vect)
{
    // toggle the pin
    pin_state = 1 - pin_state;
    if (pin_state == 0) {
        // Clear Bit - PortB2 is the internal
        // numbering in the Port register for
        // pin 10 on the Nano board
        cbi(PORTB, PORTB2);
    } else {
        // Set Bit
        sbi(PORTB, PORTB2);
    }

    // if you use the normal digitalWrite, this routine takes too long
    // which means it takes too much of the available CPU time to process.
    // That impacts the ability of the loop() function to properly
    // handle the serial port. For example, you'll see the serial output
    // hang and the frequency reading on your oscilloscope will go
    // to around 32K.
    //
    // Note: this still occurs with the cbi/sbi routines above, but
    // only at much higher frequencies, e.g. around 128Khz
    // see the spreadsheet
    //
    //  digitalWrite(pin_num, pin_state);
}

// --------------------
//! holds various timer related parameters
static struct {
    // a line is <prescalar> <ocr2a value>
    // state keeps track of where we are reading a line
    //  1: about to read prescaler1
    //  2: about to read prescaler2
    //  3: about to read prescaler3
    //  4: read a space
    //  5: reading ocr value
    //  6: set timer
    int state;

    //! prescalar: can be
    //     000 =    none
    //     001 =    1
    //     010 =    8
    //     011 =   32
    //     100 =   64
    //     101 =  128
    //     110 =  256
    //     111 = 1024
    //! holds prescaler1 cfg value
    int prescaler1;
    //! holds prescaler2 cfg value
    int prescaler2;
    //! holds prescaler3 cfg value
    int prescaler3;

    //! ocr2a value can be 0 - 255
    int ocr_val;
} timer_info;

// --------------------
//! intialize timer prescaler and OCR info
static void timer_info_init()
{
    timer_info.prescaler1 = 0;
    timer_info.prescaler2 = 0;
    timer_info.prescaler3 = 0;
    timer_info.ocr_val = 0;
}

// --------------------
//! set prescaler based on params set in
//! @param[in] ch     which prescaler value to set to
//! @param[in] psnum  which prescaler to set
void timer_info_set_prescaler(char ch, int psnum)
{
    int* prescaler = 0;
    char* name = 0;
    switch (psnum) {
    case 1:
        prescaler = &timer_info.prescaler1;
        name = "prescaler1";
        break;
    case 2:
        prescaler = &timer_info.prescaler2;
        name = "prescaler2";
        break;
    case 3:
        prescaler = &timer_info.prescaler3;
        name = "prescaler3";
        break;
    }

    switch (ch) {
    case '0':
        *prescaler = 0;
        break;
    case '1':
        *prescaler = 1;
        break;
    default:
        Serial.print("NAK unknown ");
        Serial.print(name);
        Serial.print(" char: ");
        Serial.println(ch);
        timer_info.state = 1;
        break;
    }
}

// --------------------
//! set interrupt timer parameters
void timer_info_change_timer()
{
    Serial.print("ACK ");
    Serial.print(timer_info.prescaler1, DEC);
    Serial.print(timer_info.prescaler2, DEC);
    Serial.print(timer_info.prescaler3, DEC);
    Serial.print(" ");
    Serial.println(timer_info.ocr_val, DEC);
    // TODO: check ocr_val is 0 - 255

    // calculate number of ticks (value in OCR2A)
    // the master clock is 16Mhz:
    //    1 / 16Mhz = 62.5ns per tick
    //    timeout = 1 / desired_frequency
    // timeout requires this many ticks:
    //    ticks = timeout (uS) / 62.5nS
    // number of prescalar ticks:
    //    prescalar ticks = ticks / prescaler value (1, 8, 32, etc.)

    // stop interrupts while we're changing the Timer params
    cli();

    // set prescaler
    TCCR2B = 0;
    if (timer_info.prescaler3 == 1) {
        TCCR2B |= _BV(CS20);
    }
    if (timer_info.prescaler2 == 1) {
        TCCR2B |= _BV(CS21);
    }
    if (timer_info.prescaler1 == 1) {
        TCCR2B |= _BV(CS22);
    }

    // set WGM to CTC mode (010)
    // In this mode Timer2 counts up until it matches OCR2A
    TCCR2A = _BV(WGM21);

    OCR2A = timer_info.ocr_val;

    // When the OCR2A register matches the Timer2 count, cause an interrupt
    TIMSK2 = _BV(OCIE2A);

    // enable interrupts
    sei();
}

// --------------------
//! Arduino setup
// cppcheck-suppress unusedFunction
void setup()
{
    // open the serial port at 115200 bps
    Serial.begin(115200);

    // set up pin as an output
    pinMode(pin_num, OUTPUT);

    timer_info.state = 1;
    timer_info_init();

    // set to 4Khz
    timer_info.prescaler1 = 0;
    timer_info.prescaler2 = 1;
    timer_info.prescaler3 = 0;
    timer_info.ocr_val = 249;
    timer_info_change_timer();
}

// --------------------
//! Arduino main loop
// cppcheck-suppress unusedFunction
void loop()
{
    // skip if nothing to read
    if (Serial.available() <= 0) {
        return;
    }

    // read the incoming byte:
    char ch = (char)Serial.read();
    // uncomment to debug
    // Serial.print(ch);
    switch (timer_info.state) {
    case 1:
        if (ch == 0x0D || ch == 0x0A) {
            // skip garbage
            break;
        }
        timer_info_init();

        timer_info.state = 2;
        timer_info_set_prescaler(ch, 1);
        break;
    case 2:
        timer_info.state = 3;
        timer_info_set_prescaler(ch, 2);
        break;
    case 3:
        timer_info.state = 4;
        timer_info_set_prescaler(ch, 3);
        break;
    case 4:
        if (ch == ' ') {
            timer_info.state = 5;
        } else {
            Serial.print("NAK expected space char: ");
            Serial.println(ch);
            timer_info.state = 1;
        }
        break;
    case 5:
        if (ch == 0x0D) {
            timer_info.state = 1;
            timer_info_change_timer();
        } else if (ch == 0x0A) {
            // skip
        } else if (ch >= '0' && ch <= '9') {
            timer_info.ocr_val = (timer_info.ocr_val * 10) + (ch - '0');
        } else {
            Serial.print("NAK unknown ocr2 char: 0x");
            Serial.print(ch, HEX);
            Serial.print("=");
            Serial.println(ch);
            timer_info.state = 1;
        }
        break;
    default:
        Serial.print("NAK unknown state: ");
        Serial.println(timer_info.state, HEX);
        timer_info.state = 1;
        break;
    }
}
