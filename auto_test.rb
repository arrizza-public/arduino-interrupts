# encoding: UTF-8
# frozen_string_literal: true
# ============================================================================
# Summary: run various combinations of Timer2 configurations
# ============================================================================
require 'serialport'

# --------------------
def read_response
  print 'rx: '
  loop do
    # get the next byte
    ch = @ser.getc

    # if we got nothing from the Arduino,
    # it may have timed out, try again
    next if ch.nil?

    # the response ends on a linefeed from the Arduino
    break if ch.ord.to_i == 0x0A

    if ch.ord.to_i == 0x0D
        print ' '
    else
        print ch
    end
  end
  puts
end

# >> MAIN --------------------
# open the port with 115200 81N
@port            = '/dev/ttyUSB0'
parity           = SerialPort::NONE
@ser              = SerialPort.new(@port, 115_200, 8, 1, parity)

# temporarily set the read timeout to non-blocking
@ser.read_timeout = -1
@ser.flush_input
@ser.flush_output

# reset the read_time to 1s
@ser.read_timeout = 1000

# read a couple of times to get rid of garbage
@ser.putc 0x0D
loop do
  ch = @ser.getbyte
  break if ch.nil? || ch == 0x0A
end

# must match the order of the values in the prescaler array
prescaler_codes = [0b111, 0b110, 0b101, 0b100, 0b011, 0b010, 0b001]
prescaler_multi = [1024, 256, 128, 64, 32, 8, 1]

# setup the various prescalers
[0, 1, 2, 3, 4, 5, 6].each do |i|
  prescaler = prescaler_codes[i]
  # set some OCR2A values for each prescaler
  [1, 32, 63, 95, 127, 255].each do |val|
    # note: some combinations of prescalar and
    #       OCR2A values cause the communication to hang
    #       i.e. the ISR is taking too much of the MCU
    #       processing time to be able to process
    #       incoming/outgoing characters on the Serial Port
    #
    # To fix:
    #   - change the ISR to be much more efficient
    #
    # To detect:
    #   - you'll see garbled input on your PC
    #   - usually occurs when frequency is ~36Khz or higher
    #
    # To recover, try this:
    #   - press reset button on Arduino
    #   - then press enter on your PC

    pulse_width = (val + 1) * ((1.0 / 16_000_000) * prescaler_multi[i])
    exp_freq = (1.0 / pulse_width) / 2.0
    msg = format('%03b %d', prescaler, val)
    puts sprintf("tx: SND %s pulse width:%0.3fmS freq:%0.3fHz", msg, pulse_width * 1_000, exp_freq)
    @ser.print msg
    @ser.putc 0x0D
    read_response

    puts 'Press enter when you\'re ready to continue!'
    gets
  end
end

@ser.close
